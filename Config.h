#ifndef __CONFIG_H__
#define __CONFIG_H__ 

#include <vector>
#include <string>
#include <limits.h>
#include <stdio.h>
#include <math.h>

using namespace std;

const int KMer = 4;
const int V = 256; // vector length sqr(k,4)
const string distanceFuncs = "euclidean";
// const string distanceFuncs = "pearson";

const std::string Nus[] = {"A","T","G","C"};
vector<string> GenerateNuVector(int lmer);

const string diNuVector[16] = {"AA", "AT", "AG", "AC", "TA", "TT", "TG", "TC", "GA", "GT", "GG", "GC", "CA", "CT", "CG", "CC"};

const int linesPerSpecies = 50;
const int maxKmeanLoop = 500;

const string VectorFolderName = "Results/"; //linux
// const string VectorFolderName = "D:\\Metagenomic\\2 species 1000\\Vectors\\"; //windows
const string ResultsFolderName = "Results/";
const string ResultsRunTimeFolderName = "Results ";
const string SamplesFolderName = "Samples/"; //linux
// const string SamplesFolderName = "D:\\Metagenomic\\2 species 1000\\Samples\\"; //windows

const string signalNames[] = {" - 4mer - ", " - GC - ", " - FOM - ", " - SOM - ", " - Hybrid GC_FOM - ", " - Hybrid GC_SOM - ", " - Hybrid 4mer_GC_FOM - ", " - Hybrid 4mer_GC_SOM - "};
const int signalNameIds[] = {0, 1, 2, 3, 4, 5, 6, 7};
// const int skipSignals[] = { -1, -1, -1, 3, -1, 5, -1, 7 };
const int skipSignals[] = { -1, 1, 2, 3, 4, 5, 6, 7 };
// const string signalNames[] = {" - 4mer - ", " - GC - ", " - FOM - ", " - SOM - ", " - Hybrid GC_FOM - "};
// const int signalNameIds[] = {0, 1, 2, 3, 4};
extern int signalId; // 0: 4-mer; 1: GC-Content; 2: FOM; 3 SOM
extern string fileName;
extern int fileIndex;
extern int totalSpecies;
extern string runtimes;

const string fileNames[] = {
		"test",
		// "01 - Genus Bacillus - 2 species - 50000 reads - 1000(100) bps - Exact",
		// "01 - Family Campylobacteraceae - 2 species - 50000 reads - 1000(100) bps - Exact",
		// "01 - Order Chroococcales - 2 species - 50000 reads - 1000(100) bps - Exact",
		"01 - Class Alphaproteobacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// "01 - Phylum Cyanobacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// "01 - Kingdom Bacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// "02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact",
		// "02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		// "02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact",
		// "02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact",
		// "02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		// "02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		// "03 - Genus Bifidobacterium - 4 species - 50000 reads - 1000(100) bps - Exact",
		// "03 - Family Enterobacteriaceae - 4 species - 50000 reads - 1000(100) bps - Exact",
		// "03 - Order Clostridiales - 4 species - 50000 reads - 1000(100) bps - Exact",
		// "03 - Class Betaproteobacteria - 4 species - 50000 reads - 1000(100) bps - Exact",
		// "03 - Phylum Firmicutes - 4 species - 50000 reads - 1000(100) bps - Exact",
		// "03 - Kingdom Bacteria - 4 species - 50000 reads - 1000(100) bps - Exact",
	};

const int arrClustersSize [][15] = {
		{5, 6},
		// {21887,	28113,		}, //"01 - Genus Bacillus - 2 species - 50000 reads - 1000(100) bps - Exact"
		// {27209,	22791,		}, //"01 - Family Campylobacteraceae - 2 species - 50000 reads - 1000(100) bps - Exact"
		// {35742,	14258,		}, //"01 - Order Chroococcales - 2 species - 50000 reads - 1000(100) bps - Exact"
		{20847,	29153,		}, //"01 - Class Alphaproteobacteria - 2 species - 50000 reads - 1000(100) bps - Exact"
		// {22612,	27388,		}, //"01 - Phylum Cyanobacteria - 2 species - 50000 reads - 1000(100) bps - Exact"
		// {18280,	31720,		}, //"01 - Kingdom Bacteria - 2 species - 50000 reads - 1000(100) bps - Exact"
		// {15745,	21314,	12941,	}, //"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact"
		// {14598,	18485,	16917,	}, //"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact"
		// {20588,	14657,	14755,	}, //"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact"
		// {12649,	13684,	23667,	}, //"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact"
		// {10616,	25625,	13759,	}, //"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact"
		// {27281,	14408,	8311,	}, //"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact"
		// {11795,	10936,	12427,	14842}, //"03 - Genus Bifidobacterium - 4 species - 50000 reads - 1000(100) bps - Exact"
		// {15915,	16898,	1491,	15696}, //"03 - Family Enterobacteriaceae - 4 species - 50000 reads - 1000(100) bps - Exact"
		// {13841,	14465,	10648,	11046}, //"03 - Order Clostridiales - 4 species - 50000 reads - 1000(100) bps - Exact"
		// {24730,	10729,	9838,	4703}, //"03 - Class Betaproteobacteria - 4 species - 50000 reads - 1000(100) bps - Exact"
		// {10360,	18734,	13025,	7881}, //"03 - Phylum Firmicutes - 4 species - 50000 reads - 1000(100) bps - Exact"
		// {14726,	3334,	19307,	12633}, //"03 - Kingdom Bacteria - 4 species - 50000 reads - 1000(100) bps - Exact"
	};

const int arrTotalReads[] = {
		11,
		// 50000, // "01 - Genus Bacillus - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "01 - Family Campylobacteraceae - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "01 - Order Chroococcales - 2 species - 50000 reads - 1000(100) bps - Exact",
		50000, // "01 - Class Alphaproteobacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "01 - Phylum Cyanobacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "01 - Kingdom Bacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "03 - Genus Bifidobacterium - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "03 - Family Enterobacteriaceae - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "03 - Order Clostridiales - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "03 - Class Betaproteobacteria - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "03 - Phylum Firmicutes - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 50000, // "03 - Kingdom Bacteria - 4 species - 50000 reads - 1000(100) bps - Exact",
	};

const int arrTotalSpecies[] = {
		2,
		// 2, // "01 - Genus Bacillus - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 2, // "01 - Family Campylobacteraceae - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 2, // "01 - Order Chroococcales - 2 species - 50000 reads - 1000(100) bps - Exact",
		2, // "01 - Class Alphaproteobacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 2, // "01 - Phylum Cyanobacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 2, // "01 - Kingdom Bacteria - 2 species - 50000 reads - 1000(100) bps - Exact",
		// 3, // "02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 3, // "02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 3, // "02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 3, // "02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 3, // "02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 3, // "02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		// 4, // "03 - Genus Bifidobacterium - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 4, // "03 - Family Enterobacteriaceae - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 4, // "03 - Order Clostridiales - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 4, // "03 - Class Betaproteobacteria - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 4, // "03 - Phylum Firmicutes - 4 species - 50000 reads - 1000(100) bps - Exact",
		// 4, // "03 - Kingdom Bacteria - 4 species - 50000 reads - 1000(100) bps - Exact",
	};

// const string fileNames[] = {
// // 		"1-species02-class-10000read_exact1000",
// // 		"1-species02-kingdom-10000read_exact1000",
// // 		"1-species-phylum-10000read_Exact_1000",
// // 		"2-species02-class-10000read_Exact1000",
// // 		"2-species02-kingdom-10000read_exact1000",
// // 		"r1",
// // 		"r3",
// // 		"r7",
// // 		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 10
// // 		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 20
// // 		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 30
// // 		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 40
// // 		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 50
// // 		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 1", //60
// // 		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 70
// // 		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 80
// // 		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 90
// // 		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 100
// // 		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 110
// // 		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 120
// // 		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 130
// // 		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 140
// // 		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 150
// // 		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 160
// // 		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3", //170
// // 		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 180 
// // 		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// 		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 190
// // 		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 200
// // 		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 210
// // 		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 220
// // 		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 230
// // 		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 240
// // 		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2", // 250
// // 		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3",
// // 		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1",
// // 		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2",
// // 		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3", // 260
// // 		// "01 - Kingdom Tets - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 260
// 	};

// const int arrClustersSize [][15] = {
// // 		{ 2959, 7041 }, //1-species02-class-10000read_exact1000
// // 		{ 7073, 2927 }, //1-species02-kingdom-10000read_exact1000
// // 		{ 6236, 3764 }, //1-species-phylum-10000read_Exact_1000
// // 		{ 6183, 3817 }, //2-species02-class-10000read_Exact1000
// // 		{ 6600, 3400 }, //2-species02-kingdom-10000read_exact1000
// // 		{ 42189, 40771 }, //r1
// // 		{ 47457,45810 }, //r3
// // 		{ 19473, 19291, 251709 }, //r7
// // 		{ 432, 568 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 530, 470 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 441, 559 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 415, 585 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 532, 468 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 371, 629 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 542, 458 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 509, 491 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 473, 527 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 496, 504 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 514, 486 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 507, 493 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 414, 586 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 418, 582 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 534, 466 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 511, 489 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 467, 533 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 488, 512 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 490, 510 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 511, 489 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 480, 520 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 521, 479 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 525, 475 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 514, 486 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 494, 506 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 528, 472 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 561, 439 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 537, 463 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 506, 494 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 524, 476 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 503, 497 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 517, 483 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 520, 480 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 514, 486 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 509, 491 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 521, 479 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 449, 551 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 466, 534 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 426, 574 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 481, 519 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 488, 512 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 524, 476 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 478, 522 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 492, 508 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 506, 494 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 527, 473 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 564, 436 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 467, 533 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 488, 512 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 536, 464 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 447, 553 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 434, 566 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 525, 475 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 547, 453 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 446, 554 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 558, 442 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 414, 586 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 568, 432 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 631, 369 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 440, 560 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 546, 454 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 423, 577 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 467, 533 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 457, 543 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 445, 555 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 471, 529 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 481, 519 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 425, 575 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 534, 466 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 933, 67 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 932, 68 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 473, 527 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 271, 729 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 380, 620 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 618, 382 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 465, 535 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 480, 520 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 422, 578 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 255, 745 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 433, 567 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 272, 728 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 574, 426 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 553, 447 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 502, 498 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 506, 494 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 497, 503 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 557, 443 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 104, 896 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 128, 872 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 435, 565 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 632, 368 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 381, 619 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 218, 782 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 517, 483 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 576, 424 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 437, 563 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 462, 538 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 493, 507 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 464, 536 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 395, 605 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 423, 577 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 422, 578 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 467, 533 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 558, 442 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 415, 585 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 516, 484 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 523, 477 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 467, 533 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 528, 472 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 433, 567 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 405, 595 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 591, 409 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 446, 554 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 537, 463 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 490, 510 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 454, 546 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 514, 486 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 430, 570 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 424, 576 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 514, 486 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 722, 278 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 560, 440 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 692, 308 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 591, 409 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 611, 389 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 486, 514 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 408, 592 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 508, 492 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 578, 422 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 524, 476 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 572, 428 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 574, 426 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 230, 770 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 219, 781 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 511, 489 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 517, 483 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 201, 799 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 854, 146 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 561, 439 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 810, 190 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 271, 729 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 313, 687 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 312, 688 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 454, 546 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 403, 597 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 532, 468 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 633, 367 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 381, 619 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 449, 551 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 549, 451 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 448, 552 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 527, 473 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 560, 440 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 627, 373 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 663, 337 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 468, 532 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 506, 494 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 504, 496 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 524, 476 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 607, 393 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 579, 421 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 470, 530 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 213, 787 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 179, 821 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 426, 574 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 385, 615 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 346, 654 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 504, 496 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 544, 456 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 669, 331 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 392, 608 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 673, 327 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 634, 366 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 535, 465 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 299, 701 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 284, 716 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 533, 467 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 437, 563 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 529, 471 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 454, 546 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// 		{ 406, 594 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 498, 502 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 417, 583 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 555, 445 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 634, 366 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 497, 503 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 698, 302 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 709, 291 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 537, 463 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 613, 387 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 533, 467 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 555, 445 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 856, 144 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 353, 647 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 917, 83 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 430, 570 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 568, 432 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 401, 599 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 471, 529 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 476, 524 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 499, 501 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 389, 611 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 464, 536 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 453, 547 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 288, 712 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 487, 513 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 274, 726 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 125, 875 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 156, 844 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 404, 596 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 462, 538 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 419, 581 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 560, 440 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 262, 738 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 431, 569 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 351, 649 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 447, 553 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 374, 626 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 575, 425 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 397, 603 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 444, 556 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 526, 474 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 442, 558 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 408, 592 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 540, 460 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 255, 745 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 164, 836 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 655, 345 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 719, 281 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 619, 381 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 808, 192 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 521, 479 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 554, 446 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 580, 420 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 368, 632 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 542, 458 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 606, 394 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 667, 333 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 741, 259 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 355, 645 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 450, 550 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 796, 204 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 829, 171 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 368, 632 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 595, 405 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 731, 269 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 616, 384 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 691, 309 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 421, 579 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		{ 370, 630 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		{ 437, 563 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		{ 484, 516 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		// { 2, 3 }, // "01 - Kingdom Tets - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 260
// 	};

// const int arrTotalReads[] = {
// // 		10000,	// 1-species02-class-10000read_exact1000
// // 		10000,	// 1-species02-kingdom-10000read_exact1000
// // 		10000,	// 1-species-phylum-10000read_Exact_1000
// // 		10000,	// 2-species02-class-10000read_Exact1000
// // 		10000,	// 2-species02-kingdom-10000read_exact1000
// // 		82960,	// r1
// // 		93267,	// r3
// // 		290473,	// r7
// // 		1000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		1000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		1000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		1000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// 		1000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		1000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		1000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		1000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		// 5,  //"01 - Kingdom Tets - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 260
// 	};

// const int arrTotalSpecies[] = {
// // 		2,	// 1-species02-class-10000read_exact1000
// // 		2,	// 1-species02-kingdom-10000read_exact1000
// // 		2,	// 1-species-phylum-10000read_Exact_1000
// // 		2,	// 2-species02-class-10000read_Exact1000
// // 		2,	// 2-species02-kingdom-10000read_exact1000
// // 		2,	// r1
// // 		2,	// r3
// // 		3,	// r7
// // 		2,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
// // 		2, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
// // 		2, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
// // 		2,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// 		2,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		2,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
// // 		2,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
// // 		2,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
// // 		// 2, // "01 - Kingdom Tets - 3 species - 50000 reads - 1000(100) bps - Exact - 1", // 260
// 	};

#endif