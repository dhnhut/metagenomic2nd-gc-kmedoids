#ifndef __DNASequence_h__
#define __DNASequence_h__

#include <string>

#include "Config.h"
#include "DNASignal.h"
#include "global.h"

using namespace std;

class DNASequence
{
public:
	DNASequence(void);
	DNASequence(string _sid);
	~DNASequence(void);

	std::string _DNAString;
	std::string _AdditionalDNAString;
	std::string _id;
	DNASequence* pNext;
	DNASequence* pNewNext;
	DNASequence* pCollectionNext;
	double _kmerVector[V];
	double _gcVector[V];
	double gcValue;
	double tmpGCValue;
	double vectorMedian;
	int gcClusterId;
	int clusterId;
	bool isMedoid;

	void Append(string subStr);
	string ExportDNA(void);
	string Id();
	void GenerateVector(int signal);
	void GenerateAdditionalDNAString();
	void InputVector(string vector);
	string ExportVector();
	double* Vector();
};

#endif // __DNASequence_h__