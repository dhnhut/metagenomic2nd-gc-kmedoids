#include <stdlib.h> 
#include <string> 
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "HybridSignal.h"
#include "Config.h"

using namespace std;

vector< vector<int> > signalsResult; //Binning result, assigning reads to clusters
vector<string> resultArr;
int clusterIDs[] = { 0, 1, 2, 3, 4, 5 };

vector<int> GetOrderCluster(){
	int iNumReads = arrTotalReads[fileIndex];
	vector<int> orderClusterIDs(totalSpecies);

	vector< vector<int> > clusterCount(totalSpecies);

	// temporary count for 2 species
	vector< vector<int> > swapOrders(2, vector<int>(2));
	swapOrders[0][0] = 0;
	swapOrders[0][1] = 1;
	swapOrders[1][0] = 1;
	swapOrders[1][1] = 0;
	vector< vector<int> > countResult(2, vector<int>(2));

	int countRead = 0;
	int countCluster = 0;

	cout<<"READ DONE1\n";

	for (int runtime = 0; runtime < countResult.size(); ++runtime)
	{
		for (int i = 0; i < iNumReads; ++i)
		{
			if(countRead < arrClustersSize[fileIndex][countCluster]){
				countRead++;
			}
			else {
				countCluster++;
				countRead = 0;
			}

			if(signalsResult[runtime][i] == swapOrders[runtime][countCluster])
				countResult[runtime][countCluster]++;
		}
	}

	cout<<"READ DONE2\n";

	int maxResult = 0;
	int maxIndex = 0;
	for (int resultIndex = 0; resultIndex < countResult.size(); ++resultIndex)
	{
		vector<int> result = countResult[resultIndex];
		int currentResult = 0;
		for (int i = 0; i < result.size(); ++i)
		{
			currentResult += i;
		}

		if(currentResult > maxResult) {
			maxResult = currentResult;
			maxIndex = resultIndex;
		}
	}

	cout<<"READ DONE3\n";
	cout<<"MaxIndex: "<<maxIndex<<"\n";

	return countResult[maxIndex];

}

void Hybrid(){
	cout<<"READ DONE-1\n";
	vector<int> clusterID = GetOrderCluster();
	cout<<"READ DONE4\n";
}

void ReadFile(){

	for (int i = 0; i < signalsResult.size(); ++i)
	{
		signalsResult[i].clear();

		int iNumClust = totalSpecies;
		int iNumReads = arrTotalReads[fileIndex];

		string fullFileName = ResultsRunTimeFolderName + runtimes + "/" + fileName + hybridSignals[i] + "result.rawdata";
		const char *cstr1 = fullFileName.c_str();

		ifstream myLogFile(cstr1);

		if(!myLogFile)
		{
			cout<<"Could not open file " << fullFileName << endl;
			return;
		}

		cout<<endl<<"Reading file";
		std::string myBuf;

		while (!myLogFile.eof())	
		{
			getline(myLogFile, myBuf); //get a string in a new line
			int currPos=-1;
			int nextPos;
			nextPos=myBuf.find(",", currPos+1);
			while(nextPos != std::string::npos)
			{
				string myStr;
				myStr=myBuf.substr(currPos + 1, nextPos - currPos -1);
				const char *cstr = myStr.c_str();
				short int clusName=atoi(cstr);
				
				signalsResult[i].push_back(clusName);
				currPos=nextPos;
				nextPos=myBuf.find(",", currPos+1);			
			}
		}

		if(iNumReads != signalsResult[i].size())
		{
			cout << endl << "NumOfRead: " << iNumReads;
			cout << endl << "signalsResult: " << signalsResult[i].size();
			cout << endl <<"Number of reads read is not right!" << endl;
			return;
		}
		cout<<endl<<"Number of reads: "<<signalsResult[i].size();

		myLogFile.close();
	}

}

void HybridResult(){

	ReadFile();

	Hybrid();

}
