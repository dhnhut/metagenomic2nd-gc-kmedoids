#ifndef __DNAKMedoids_H__
#define __DNAKMedoids_H__

#include "global.h"
#include "Config.h"
#include "DNACenter.h"
#include "DNACollection.h"
#include "Distance.h"
#include "DistanceList.h"
#include "DNAKMedoids.h"

void DNA_KMedoids(DNACollection* collection);
void GenDistanceMatrix(DNACollection* collection);
vector< vector<double> > ReadDistanceMatrix();

#endif