#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits.h>
#include <math.h> 
#include <cmath> 
#include <float.h> 
#include <fstream>
#include <sstream>
#include "DNAKMedoids.h"

struct MedoidCenter
{
	DNASequence* point;
	double gcValue;
	double totalGCVal;
	int totalElement;
	double mean;
	double variance;
	double standardError;
	double maxStandardError;

	MedoidCenter(){
		totalGCVal = 0;
		totalElement = 0;
		mean = 0;
		variance = 0;
		standardError = 0;
		maxStandardError = 0;
		point = NULL;
	}
};

double euclideanKmerDistance(vector<double> stVec, double ndVec[]){
	double distance = 0;
	for (int i = 0; i < V; i++)
	{
		double a = stVec[i];
		double b = ndVec[i];
		double dis = a - b;
		distance += dis * dis;
	}

	return distance;
}

int nearesetKmerCenter(vector<vector<double> > kmerVector, double readVec[]) {

	vector<double> distances(totalSpecies, 0);
	for (int i = 0; i < totalSpecies; ++i)
	{
		distances[i] = euclideanKmerDistance(kmerVector[i], readVec);
	}

	double minIndex = 0;
	double minValue = 0;
	for (int i = 0; i < totalSpecies; ++i)
	{
		if(minValue < distances[i]) {
			minIndex = i;
			minValue = distances[i];
		}
	}

	return minIndex;
}

double CalculateDistance(DNASequence* first, DNASequence* second) {
	double distance = 0;

	for (int i = 0; i < V; i++)
	{
		double a = first -> _gcVector[i];
		double b = second -> _gcVector[i];
		double dis = a - b;
		distance += dis * dis;
	}

	return sqrt(distance);
}

double calculateGCDistance(double first, double second) {
	double distance = 0;

	double dis = first - second;

	distance += dis * dis;

	return abs(first - second);
}

void OutlierClutering(DNACollection* collection) {

	cout<<"STEP 1"<<endl;
	// Step 1 init kmer vectors
	vector<vector<double> > kmerVectors(totalSpecies, vector<double>(V, 0));
	vector<int> clusterNumberOfSeq(totalSpecies, 0);

	cout<<"STEP 2"<<endl;
	// step 2, calculate clusters kmer vector
	DNASequence* currentSeq = collection -> pFirst;
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{
		int clusterId = currentSeq -> clusterId;
		if(clusterId != -1) {
			for (int j = 0; j < V; ++j)
			{
				kmerVectors[clusterId][j] += currentSeq -> _kmerVector[j];
			}
			++clusterNumberOfSeq[clusterId];
		}

		currentSeq = currentSeq -> pCollectionNext;
	}

	for (int i = 0; i < totalSpecies; ++i)
	{
		for (int j = 0; j < V; ++j)
		{
			kmerVectors[i][j] /= clusterNumberOfSeq[i];
		}
	}

	cout<<"STEP 3"<<endl;
	currentSeq = collection -> pFirst;
	// step 3, assign each unclustered read to nearest cluster
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{
		int clusterId = currentSeq -> clusterId;
		if(clusterId == -1) {
			clusterId = nearesetKmerCenter(kmerVectors, currentSeq -> _kmerVector);
			// cout<<"Oulier: "<<currentSeq -> _id<<endl;
		}

		currentSeq = currentSeq -> pCollectionNext;
	}
}

void OutlierDetect(DNACollection* collection) {
	vector<MedoidCenter> pCenters(totalSpecies);
	DNASequence* currentSeq = collection -> pFirst;

	// calculate totalGC and total member of each cluster
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{
		currentSeq -> gcValue = currentSeq -> _gcVector[0];
		if(currentSeq -> isMedoid) {
			pCenters[currentSeq -> gcClusterId].gcValue = currentSeq -> gcValue;
			pCenters[currentSeq -> gcClusterId].point = currentSeq;
		}

		pCenters[currentSeq -> gcClusterId].totalGCVal += currentSeq -> gcValue;
		pCenters[currentSeq -> gcClusterId].totalElement += 1;

		currentSeq = currentSeq -> pCollectionNext;
	}

	// calculate mean of each center
	for (int i = 0; i < totalSpecies; ++i)
	{
		pCenters[i].mean = pCenters[i].totalGCVal / pCenters[i].totalElement;
	}

	// calculate variance of each center
	currentSeq = collection -> pFirst;
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{
		pCenters[currentSeq -> gcClusterId].variance += 
			pow(
				currentSeq -> gcValue - pCenters[currentSeq -> gcClusterId].mean, 2
			);

		// cout<<"CENTER: " << currentSeq -> gcClusterId << " ; ";
		// cout<<"SEQ: " << currentSeq -> _id << " ; ";
		// cout<<"Variance: " << pCenters[currentSeq -> gcClusterId].variance<< endl;

		currentSeq = currentSeq -> pCollectionNext;
	}

	// standar deviation
	for (int i = 0; i < totalSpecies; ++i)
	{
		pCenters[i].standardError = 
			sqrt(pCenters[i].variance / pCenters[i].totalElement);

		pCenters[i].maxStandardError = 5 * pCenters[i].standardError;
	}

	// // View all element
	// currentSeq = collection -> pFirst;
	// for (int i = 0; i < collection -> numOfSequence; ++i)
	// {
	// 	cout<<"ID: " << currentSeq -> _id << "; ";
	// 	cout<<"Cluster: " << currentSeq -> gcClusterId << "; ";
	// 	cout<<"GC: " << currentSeq -> _gcVector[0] << "; ";
	// 	if(currentSeq -> isMedoid)
	// 		cout<<" IS MEDOID !!! ";
	// 	cout<<endl;
	// 	currentSeq = currentSeq -> pNext;
	// }

	// // View all centers
	// for (int i = 0; i < totalSpecies; ++i)
	// {
	// 	cout<<"ID: "<<pCenters[i].point -> _id<<endl;
	// 	cout<<"totalGCVal: "<<pCenters[i].totalGCVal<<endl;
	// 	cout<<"totalElement: "<<pCenters[i].totalElement<<endl;
	// 	cout<<"mean: "<<pCenters[i].mean<<endl;
	// 	cout<<"variance: "<<pCenters[i].variance<<endl;
	// 	cout<<"standardError: "<<pCenters[i].standardError<<endl;
	// }

	// remove outlier element
	currentSeq = collection -> pFirst;
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{

		double distance = calculateGCDistance(
			currentSeq -> gcValue, 
			pCenters[currentSeq -> gcClusterId].gcValue
		);
		
		if (distance < pCenters[currentSeq -> gcClusterId].maxStandardError) {
			currentSeq -> clusterId = currentSeq -> gcClusterId;
		}

		// cout<<endl;
		// cout<<"Center: "<<pCenters[currentSeq -> gcClusterId].point -> _id<<endl;
		// cout<<"Center GC: "<<pCenters[currentSeq -> gcClusterId].gcValue<<endl;
		// cout<<"SEQ: "<<currentSeq -> _id <<endl;
		// cout<<"GC: "<<currentSeq -> gcValue<<endl;
		// cout<<"distance: "<<distance<<endl;
		// cout<<"clusterId: "<<currentSeq -> clusterId<<endl;

		currentSeq = currentSeq -> pCollectionNext;
	}

	cout << "DONE OUTLIER" << endl;
}

struct SwapInfo
{
	double minSwapCost;
	int mediodIndex;
	int sequenceIndex;
};

vector<int> Random_Medoids(DNACollection* collection){
	int totalReads = arrTotalReads[fileIndex];
	vector<int> pMedoids(totalSpecies);

	for (int i = 0; i < totalSpecies; ++i)
	{
		srand(time(NULL));
		pMedoids[i] = rand() % totalReads;  //number between 1 and totalReads
		//make sure no duplicate center
		for (int j = 0; j < i; ++j)
		{
			if (pMedoids[i] == pMedoids[j])
			{
				--i;
				break;
			}
		}
	}
	return pMedoids;
}

double FindDistance(int first, int second, vector< vector<double> > *matrix) {
	vector< vector<double> > &vr = *matrix; //Create a reference
	if(first < second)
		return (*matrix)[first][second];
	else
		return (*matrix)[second][first];
}

DNASequence* GetReadByIndex(DNACollection* collection, int pMedoid){
	int totalReads = collection -> numOfSequence;

	// get current medoid sequences
	DNASequence* currentSeq = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		// Check if current sequence is medoid or not
		if (i == pMedoid)
		{
			// if sequence is mediod, 
			return currentSeq;
		}

		currentSeq = currentSeq -> pNext;
	}

	return NULL;
}

// assign medoids, calculate min distance between each read to each medoids,
// then assign read to nearest cluster
double AssignToMedoids(DNACollection* collection, vector<int> pMedoids, 
	vector< vector<double> > *matrix, bool isTmp) {

	int totalReads = collection->numOfSequence;
	double totalCost = 0;

	// cout<<"pMedoids 1: "<<pMedoids[0]<<endl;
	// cout<<"pMedoids 2: "<<pMedoids[1]<<endl;

	DNASequence* currentSeq = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		// Check if current sequence is medoid or not
		for (int j = 0; j < totalSpecies; ++j)
		{
			if (i == pMedoids[j])
			{
				// if sequence is mediod, 
				currentSeq -> isMedoid = true;

				if(isTmp){
					currentSeq -> tmpGCValue = j;
				}
				else {
					currentSeq -> gcClusterId = j;
				}

				break;
			}
			else {
				currentSeq -> isMedoid = false;
			}
		}

		// if current sequence is not medoid,
		// calculate distance from it to all medoids,
		// then assign it to closest medoid cluster
		if(!currentSeq -> isMedoid){
			double minDistance = DBL_MAX;
			int minIndex = -1;
			for (int p = 0; p < totalSpecies; ++p)
			{
				// double distance = CalculateDistance(medoids[p], currentSeq);
				double distance = FindDistance(pMedoids[p], i, matrix);
				if (distance < minDistance){
					minDistance = distance;
					minIndex = p;
				}
			}
			totalCost += minDistance;

			// cout<<"seq: " << currentSeq -> _id << "; ";
			// cout<<"Nearest medoids: " << minIndex << "; ";
			// cout<<"minDistance: " << minDistance << "; "<<endl;

			if(isTmp){
				currentSeq -> tmpGCValue = minIndex;
			}
			else {
				currentSeq -> gcClusterId = minIndex;
			}
		}
		currentSeq = currentSeq -> pNext;
	}

	// currentSeq = collection -> pFirst;
	// cout<<"AssignToMedoids: "<<endl;
	// for (int i = 0; i < totalReads; ++i)
	// {
	// 	cout<<"ID: " << currentSeq -> _id << "; ";
	// 	cout<<"Cluster: " << currentSeq -> gcClusterId << "; ";
	// 	currentSeq = currentSeq -> pNext;
	// }
	// cout<<endl;

	return totalCost;
}

SwapInfo SwapMedoids(DNACollection* collection, vector<int> pMedoids, 
	vector< vector<double> > *matrix) {

	SwapInfo swapInfo;
	swapInfo.minSwapCost = DBL_MAX;
	swapInfo.mediodIndex = 0;
	swapInfo.sequenceIndex = 0;

	DNASequence* medoids[totalSpecies];
	for (int i = 0; i < totalSpecies; ++i)
	{
		medoids[i] = GetReadByIndex(collection, pMedoids[i]);
	}

	int totalReads = collection->numOfSequence;
	// for each medoid
	for (int m = 0; m < pMedoids.size(); ++m)
	{
		DNASequence* currentSeq = collection -> pFirst;
		for (int non_m = 0; non_m < totalReads; ++non_m)
		{
			if(!currentSeq -> isMedoid){
				// swap current medoid
				vector<int> swapMedoids = pMedoids;
				swapMedoids[m] = non_m;
				DNASequence* swapMed = GetReadByIndex(collection, non_m);
				int tmpGCCluster = swapMed -> gcClusterId;
				swapMed -> gcClusterId = medoids[m] -> gcClusterId;
				swapMed -> isMedoid = true;
				medoids[m] -> isMedoid = false;

				// caculate total distance for this swapping
				double dis = AssignToMedoids(collection, swapMedoids, matrix, true);
				if(dis < swapInfo.minSwapCost){
					swapInfo.minSwapCost = dis;
					swapInfo.mediodIndex = m;
					swapInfo.sequenceIndex = non_m;
				}

				// revert swapping
				swapMed -> gcClusterId = tmpGCCluster;
				medoids[m] -> isMedoid = true;
				swapMed -> isMedoid = false;
			}
			currentSeq = currentSeq -> pNext;
		}
	}

	// DNASequence* currentSeq = collection -> pFirst;
	// cout<<"SwapMedoids: "<<endl;
	// cout<<"swapInfo.minSwapCost: "<<swapInfo.minSwapCost<<endl;
	// cout<<"swapInfo.mediodIndex: "<<swapInfo.mediodIndex<<endl;
	// cout<<"swapInfo.sequenceIndex: "<<swapInfo.sequenceIndex<<endl;
	// for (int i = 0; i < totalReads; ++i)
	// {
	// 	cout<<"ID: " << currentSeq -> _id << "; ";
	// 	cout<<"Cluster: " << currentSeq -> gcClusterId << "; ";
	// 	cout<<"GC: " << currentSeq -> _gcVector[0] << "; ";
	// 	if(currentSeq -> isMedoid)
	// 		cout<<" IS MEDOID !!! ";
	// 	cout<<endl;
	// 	currentSeq = currentSeq -> pNext;
	// }

	return swapInfo;
}

void DNA_KMedoids(DNACollection* collection) {
	
	vector< vector<double> > matrix = ReadDistanceMatrix();

	double minTotalCost = 0;

	// step 1: random choice medoids
	vector<int> pMedoids = Random_Medoids(collection);
	// vector<int> pMedoids;
	// pMedoids.push_back(1);
	// pMedoids.push_back(10);
	
	bool isChanged = true;
	int loop = 0;

	// step 2: assign collection to medoids
	minTotalCost = AssignToMedoids(collection, pMedoids, &matrix, false);

	// DNASequence* currentSeq = collection -> pFirst;
	// cout<<"Init: "<<endl;
	// cout<<"minTotalCost: "<<minTotalCost<<endl;
	// for (int i = 0; i < collection -> numOfSequence; ++i)
	// {
	// 	cout<<"ID: " << currentSeq -> _id << "; ";
	// 	cout<<"Cluster: " << currentSeq -> gcClusterId << "; ";
	// 	cout<<"GC: " << currentSeq -> _gcVector[0] << "; ";
	// 	if(currentSeq -> isMedoid)
	// 		cout<<" IS MEDOID !!! ";
	// 	cout << endl;
	// 	currentSeq = currentSeq -> pNext;
	// }

	do
	{
		// step 3: swap each medoid to non-medoid
		SwapInfo swapInfo = SwapMedoids(collection, pMedoids, &matrix);
		// step 4: Select lowest cost
		cout<<"\nLoop: "<< loop << " - cost: " << minTotalCost << " - swap: " << swapInfo.minSwapCost <<"\n";
		if(swapInfo.minSwapCost < minTotalCost){
			// swap to lowest change
			DNASequence* current = GetReadByIndex(collection, pMedoids[swapInfo.mediodIndex]);
			DNASequence* swap = GetReadByIndex(collection, swapInfo.sequenceIndex);

			current -> gcClusterId = swap -> gcClusterId;
			current -> isMedoid = false;
			swap -> isMedoid = true;

			minTotalCost = swapInfo.minSwapCost;
			pMedoids[swapInfo.mediodIndex] = swapInfo.sequenceIndex;
		}
		else {
			isChanged = false;
		}

		AssignToMedoids(collection, pMedoids, &matrix, false);
		loop++;
	// step 5: check if medoids are changed or not to go back step 2 or exits
	} while (isChanged);

	// currentSeq = collection -> pFirst;
	// cout<<"END LOOP: "<<endl;
	// cout<<"minTotalCost: "<<minTotalCost<<endl;
	// for (int i = 0; i < collection -> numOfSequence; ++i)
	// {
	// 	cout<<"ID: " << currentSeq -> _id << "; ";
	// 	cout<<"Cluster: " << currentSeq -> gcClusterId << "; ";
	// 	cout<<"GC: " << currentSeq -> _gcVector[0] << "; ";
	// 	if(currentSeq -> isMedoid)
	// 		cout<<" IS MEDOID !!! ";
	// 	cout << endl;
	// 	currentSeq = currentSeq -> pNext;
	// }

	// DNASequence* currentSeq = collection -> pFirst;
	// for (int i = 0; i < collection -> numOfSequence; ++i)
	// {
	// 	currentSeq -> clusterId = currentSeq -> gcClusterId;
	// 	currentSeq = currentSeq -> pNext;
	// }	

	OutlierDetect(collection);
	OutlierClutering(collection);
}

void GenDistanceMatrix(DNACollection* collection){
	string fullFileName = ResultsFolderName + fileName + signalNames[signalId] + "distances.matrix";
	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);

	int totalReads = arrTotalReads[fileIndex];
	// int totalReads = 5;
	DNASequence* currentRowSeq = collection -> pFirst;
	for (int row = 0; row < totalReads; ++row)
	{
		DNASequence* currentColSeq = collection -> pFirst;

		for (int visitedRow = 0; visitedRow <= row; ++visitedRow)
		{
			currentColSeq = currentColSeq -> pNext;
			// matrix[row][visitedRow] = 0;
			myfile << "0,";
		}

		for (int col = row + 1; col < totalReads; ++col)
		{
			double distance = CalculateDistance(currentRowSeq, currentColSeq);
			// matrix[row][col] = distance;
			ostringstream ss;
			ss << distance;
			myfile << ss.str() + ",";
			currentColSeq = currentColSeq -> pNext;
		}
		myfile << "\n";
		currentRowSeq = currentRowSeq -> pNext;
	}

	myfile.close();
}

vector< vector<double> > ReadDistanceMatrix(){
	int totalReads = arrTotalReads[fileIndex];

	vector< vector<double> > matrix(totalReads, vector<double>(totalReads));
	string fullFileName = ResultsFolderName + fileName + signalNames[signalId] + "distances.matrix";
	const char *cstr = fullFileName.c_str();

	ifstream myFile(cstr);
	if(!myFile)
	{
		cout << "Cannot open input Matrix file: " << fileName;
		return matrix;
	}

	for (int row = 0; row < totalReads; ++row)
	{
		string myBuf;
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			istringstream iss(myBuf);
			vector<string> tokens;
			string token;

			int i = 0;
			while(std::getline(iss, token, ',')) {
				matrix[row][i] = atof(token.c_str());
				i++;
			}
		}
	}

	return matrix;
	myFile.close();
}