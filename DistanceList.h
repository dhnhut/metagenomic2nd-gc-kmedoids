#ifndef __DistanceList_H__
#define __DistanceList_H__

#include "global.h"
#include "Distance.h"

class DistanceList
{
public:
	DistanceList(void);
	DistanceList(int _numOfElement);
	~DistanceList(void);

	int numOfElement;
	double minDistance;
	int minDistanceIndex;
	Distance *pFirst;
	Distance *pLast;
	Distance *pMin;

	void CreateList();
};

#endif