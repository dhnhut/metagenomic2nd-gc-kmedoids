#ifndef __DNACollection_H__
#define __DNACollection_H__

#include "global.h"
#include <iostream>

#ifndef _DNASequence_
	#include "DNASequence.h"
#endif

using namespace std;

class DNACollection
{
public:
	DNACollection(void);
	~DNACollection(void);

	DNASequence* pFirst;
	DNASequence* pLast;
	int numOfSequence;

	void AppendDNA(DNASequence* sequence);
	void ExportClusteringFile();
	void Export4merVectorsFile();
	void ExportGCVectorsFile();
	void ExportChartFile();
	string ExportSubChartString(vector<string> result, string readColors, bool isSubstring);
};

#endif