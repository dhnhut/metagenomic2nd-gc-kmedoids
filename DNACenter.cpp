#include "DNACenter.h"
#include <iostream>


DNACenter::DNACenter(void)
{
	pLast = NULL;
	pFrist = NULL;
}

DNACenter::~DNACenter(void)
{
	// DNASequence *current = pFrist;
	// while (current != NULL)
	// {
	// 	DNASequence *tmp = current;
	// 	current = current -> pNext;
	// 	delete tmp;
	// }
}

DNACenter::DNACenter(DNASequence _point) {
	point = _point;
	pLast = NULL;
	pFrist = NULL;
}

void DNACenter::Add(DNASequence *p, int clusterId) {
	p -> clusterId = clusterId;
	if(pFrist == NULL) {
		pFrist = p;
		pLast = p;
	} else {
		pLast -> pNewNext = p;
		pLast = p;
	}
}

void DNACenter::UpdateCenter() {
	if(pFrist == NULL)
		return;
	int count = 0;
	double sum[V];
	for (int i = 0; i < V; i++)
	{
		sum[i] = 0;
	}
	DNASequence *pTmp = pFrist;
	do {
		for(int i = 0; i < V; i++) {
			sum[i] += pTmp -> _gcVector[i];
		}
		pTmp = pTmp -> pNewNext;
		count++;
	} while (pTmp != NULL);
	for(int i = 0; i < V; i++) {
		point._gcVector[i] = sum[i]/count;
	}
}

void DNACenter::UpdateCluster(DNACenter *pCenter) {
	pCenter -> pFrist = pFrist;
	pCenter -> pLast = pLast;

	if(pFrist == NULL)
		return;
	DNASequence *pTmp = pFrist;
	do {
		pTmp -> pNext = pTmp -> pNewNext;
		pTmp -> pNewNext = NULL;
		pTmp = pTmp -> pNext;
	} while (pTmp != NULL);
}

void DNACenter::ExportCluster()
{
	DNASequence *current;
	current = pFrist;
	printf("\nCLUSTER MEMBERS: ");
	int numOfElement = 0;
	while (current != NULL)
	{
		cout << current -> _id;
		//cout << current -> _id << ": " << current -> ExportVector();
		current = current -> pNext;
		numOfElement++;
		if(current != NULL)
			cout << " - ";
	}
	printf("\nTOTAL MEMBER: %d \n", numOfElement);
}
