CXX=g++

SRCS=Main.cpp \
	Config.cpp \
	DistanceCalculator.cpp \
	Distance.cpp \
	DistanceList.cpp \
	evaluation.cpp \
	DNASignal.cpp \
	DNASequence.cpp \
	DNACollection.cpp \
	DNACenter.cpp \
	DNAReadFile.cpp \
	DNAKMedoids.cpp
	
OBJS=${SRCS:.cpp=.o}

TARGET=Metagenomic

CPPFLAGS=-O -g
LDFLAGS=-O2 -g

all: ${TARGET}

Metagenomic: ${OBJS}
	${CXX} ${LDFLAGS} -o $@ $^

.cpp.o:
	${CXX} ${CPPFLAGS} -c -o $@ $^

clean:
	rm -rf ${TARGET} ${OBJS}	
	
