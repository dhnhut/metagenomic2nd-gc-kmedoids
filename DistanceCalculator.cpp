#include <iostream>
#include "math.h"
#include "DNASequence.h"
#include "DistanceCalculator.h"

using namespace std;

void quickSort(double arr[], int left, int right) {
	int i = left, j = right;
	double tmp;
	double pivot = arr[(left + right) / 2];
 
	/* partition */
	while (i <= j) {
		while (arr[i] < pivot)
			i++;
		while (arr[j] > pivot)
			j--;
		if (i <= j) {
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
			i++;
			j--;
		}
	};
 
	/* recursion */
	if (left < j)
			quickSort(arr, left, j);
	if (i < right)
			quickSort(arr, i, right);
}

double findMedian(double arr[], int left, int right){
	double copiedArr[V] = { };

	for (int i = 0; i < V; ++i)
	{
		copiedArr[i] = arr[i];
	}

	quickSort(copiedArr, left, right);

	int pos = V / 2;

	if( V % 2 == 0)
		return ( copiedArr[pos - 1] + copiedArr[pos] ) / 2;
	return copiedArr[pos - 1];
}

double euclideanDistance(double stVec[], double ndVec[]){
	double distance = 0;
	for (int i = 0; i < V; i++)
	{
		double a = stVec[i];
		double b = ndVec[i];
		double dis = a - b;
		distance += dis * dis;
	}

	return distance;
}

double pearsonDistance(double stVec[], double ndVec[], double stMedian, double ndMedian) {

	double numerator = 0;
	double stDenominator = 0;
	double ndDenominator = 0;

	for (int i = 0; i < V; ++i)
	{
		double subX = stVec[i] - stMedian;
		double subY = ndVec[i] - ndMedian;

		numerator += subX * subY;

		stDenominator += pow(subX, 2);
		ndDenominator += pow(subY, 2);
	}

	return numerator / ( sqrt( stDenominator ) * sqrt( ndDenominator ) );
}

double calculateDistance(string distanceName, double stVec[], double ndVec[],
	double stMedian, double ndMedian) {

	if (distanceName == "euclidean")
	{
		return euclideanDistance(stVec, ndVec);
	}

	return pearsonDistance(stVec, ndVec, stMedian, ndMedian);
}