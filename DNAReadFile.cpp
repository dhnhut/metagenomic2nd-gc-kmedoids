#include "DNAReadFile.h"

DNACollection ReadDNA()
{
	DNACollection collection = DNACollection();
	DNASequence* sequence = new DNASequence();

	string fullFileName = SamplesFolderName + fileName + ".fna";
	const char *cstr = fullFileName.c_str();

	ifstream myFile(cstr);
	if(!myFile)
	{
		cout << "Cannot open input Fasta file: " << fileName;
		return collection;
	}
	string myBuf;
	int countSeqs = 0;
	while (!myFile.eof())
	{
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			//cout<<myBuf;
			if(myBuf[0]== '>')//got new sequence
			{
				countSeqs++;
				// if(countSeqs % 1000 == 0) {
				// 	cout << "SEQ: " << countSeqs << " \n ";}

				if(sequence->Id() != ""){
					sequence->GenerateVector(signalId);
					//cout << "SEQUENCE " << sequence->Id() << sequence->ExportDNA() << "\n";
					collection.AppendDNA(sequence);
				}
				int pos = myBuf.find(" ");
				sequence = new DNASequence(myBuf.substr(1, pos));
				//store the old sequence first
				//sequence.Append(myBuf);
					
			}
			else//got next subsequence line
			{
				sequence->Append(myBuf);
			}
		}
	}

	myFile.close();

	//store the last sequence
	sequence->GenerateVector(signalId);
	collection.AppendDNA(sequence);
	cout<<"\n";
	return collection;
}

DNACollection ReadKmerVector()
{
	DNACollection collection = DNACollection();
	DNASequence* sequence = new DNASequence();

	string fullFileName = VectorFolderName + fileName + " - 4mer - Vectors.txt";
	const char *cstr = fullFileName.c_str();

	ifstream myFile(cstr);
	if(!myFile)
	{
		cout << "Cannot open input Vector file: " << fileName;
		return collection;
	}

	string myBuf;
	int countSeqs = 0;
	while (!myFile.eof())
	{
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			sequence = new DNASequence();
			sequence -> InputVector(myBuf);

			collection.AppendDNA(sequence);
			//store the old sequence first
			//sequence.Append(myBuf);

			countSeqs++;
			// if(countSeqs % 1000 == 0) {
			// 	cout << "SEQ : " << countSeqs << "\n";}
		}
	}

	myFile.close();
	return collection;
}

DNACollection ReadGCVector()
{
	DNACollection collection = DNACollection();
	DNASequence* sequence = new DNASequence();

	string fullFileName = VectorFolderName + fileName + " - GC - Vectors.txt";
	const char *cstr = fullFileName.c_str();

	ifstream myFile(cstr);
	if(!myFile)
	{
		cout << "Cannot open input Vector file: " << fileName;
		return collection;
	}

	string myBuf;
	int countSeqs = 0;
	while (!myFile.eof())
	{
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			sequence = new DNASequence();
			sequence -> InputVector(myBuf);

			collection.AppendDNA(sequence);
			//store the old sequence first
			//sequence.Append(myBuf);

			countSeqs++;
			// if(countSeqs % 1000 == 0) {
			// 	cout << "SEQ : " << countSeqs << "\n";}
		}
	}

	myFile.close();
	return collection;
}