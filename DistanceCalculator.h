#ifndef __DISTANCECALCULATOR_H__
#define __DISTANCECALCULATOR_H__

#include <cstdlib>
#include "Config.h"

double calculateDistance(string distanceName, double stVec[], double ndVec[],
	double stMedian, double ndMedian);

double euclideanDistance(double* stVec, double* ndVec);

double pearsonDistance(double* stVec, double* ndVec);

double findMedian(double arr[], int left, int right);

#endif