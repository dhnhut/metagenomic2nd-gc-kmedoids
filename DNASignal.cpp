#include <vector>
#include <string>
#include <iostream>
#include <map>
#include <math.h> 
#include "DNASignal.h"

using namespace std;

vector<string> kmer3NuVector = GenerateNuVector(3);
vector<string> kmer4NuVector = GenerateNuVector(4);

void GenerateKMerVector(double *_vector, string _DNAString, string _AdditionalString)
{
	double dividor = (_DNAString.size() - KMer + 1) * 2;

	for(int i = 0; i < V; i++)
	{
		for(int j = 0; j < _DNAString.size() - 1; j++)
		{
			if(_DNAString.compare(j, KMer, kmer4NuVector[i]) == 0)
			{
				_vector[i]++;
			}
			if(_AdditionalString.compare(j, KMer, kmer4NuVector[i]) == 0)
			{
				_vector[i]++;
			}
		}
		_vector[i] = _vector[i]/dividor;
	}
}

void GenerateGCContentVector(double *_vector, string _DNAString, string _AdditionalString)
{
	double GC = 0;
	for (int i = 0; i < _DNAString.length(); i++)
	{
		if(_DNAString[i] == 'G' || _DNAString[i] == 'C' || _AdditionalString[i] == 'G' || _AdditionalString[i] == 'C')
			GC++;
	}
	_vector[0] = GC / _DNAString.length();
}

void GenerateFOMVector(double *_vector, string _DNAString, string _AdditionalString)
{
	map<string, int> monoNuMap;
	map<string, int> diNuMap;
	map<string, int> tetraNuMap;

	//init
	for (int i = 0; i < 4; i++)
	{
		monoNuMap[Nus[i]] = 0;
	}

	for (int i = 0; i < 16; i++)
	{
		diNuMap[diNuVector[i]] = 0;
	}

	for (int i = 0; i < kmer4NuVector.size(); i++)
	{
		tetraNuMap[kmer4NuVector[i]] = 0;
	}

	//count frequency
	for (int i = 0; i < _DNAString.length(); i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if(_DNAString.compare(i, 1, Nus[j]) == 0){
				++monoNuMap[Nus[j]];
				break;
			}
		}
	}

	//
	for (int i = 0; i < _DNAString.length() - 1; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			if(_DNAString.compare(i, 2, diNuVector[j]) == 0){
				++diNuMap[diNuVector[j]];
				break;
			}
		}
		for (int j = 0; j < 16; j++)
		{
			if(_AdditionalString.compare(i, 2, diNuVector[j]) == 0){
				++diNuMap[diNuVector[j]];
				break;
			}
		}
	}

	for (int i = 0; i < _DNAString.length() - 3; i++)
	{
		for (int j = 0; j < kmer4NuVector.size(); j++)
		{
			if(_DNAString.compare(i, KMer, kmer4NuVector[j]) == 0){
				++tetraNuMap[kmer4NuVector[j]];
				break;
			}
		}
		for (int j = 0; j < kmer4NuVector.size(); j++)
		{
			if(_AdditionalString.compare(i, KMer, kmer4NuVector[j]) == 0){
				++tetraNuMap[kmer4NuVector[j]];
				break;
			}
		}
	}

	for (int i = 0; i < V; i++)
	{
		string XYZW = kmer4NuVector[i]; //f XYZW
		string Y = kmer4NuVector[i].substr(1,1);
		string Z = kmer4NuVector[i].substr(2,1);

		string XY = kmer4NuVector[i].substr(0,2);
		string YZ = kmer4NuVector[i].substr(1,2);
		string ZW = kmer4NuVector[i].substr(2,2);
		double dividor = (diNuMap[XY] * diNuMap[YZ] * diNuMap[ZW]);
		if (dividor == 0)
		{
			_vector[i] = 0;
		}
		else
		{
			_vector[i] = (double)(tetraNuMap[XYZW] * monoNuMap[Y] * monoNuMap[Z])/dividor;
		}
	}
	
}

void GenerateSOMVector(double *_vector, string _DNAString, string _AdditionalString)
{
	map<string, int> diNuMap;
	map<string, int> triNuMap;
	map<string, int> tetraNuMap;

	//init
	for (int i = 0; i < 16; i++)
	{
		diNuMap[diNuVector[i]] = 0;
	}

	for (int i = 0; i < 64; i++)
	{
		triNuMap[kmer3NuVector[i]] = 0;
	}

	for (int i = 0; i < kmer4NuVector.size(); i++)
	{
		tetraNuMap[kmer4NuVector[i]] = 0;
	}

	//count frequency
	for (int i = 0; i < _DNAString.length() - 1; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			if(_DNAString.compare(i, 2, diNuVector[j]) == 0){
				++diNuMap[diNuVector[j]];
				break;
			}
		}

		for (int j = 0; j < 16; j++)
		{
			if(_AdditionalString.compare(i, 2, diNuVector[j]) == 0){
				++diNuMap[diNuVector[j]];
				break;
			}
		}
	}
	for (int i = 0; i < _DNAString.length() - 2; i++)
	{
		for (int j = 0; j < 64; j++)
		{
			if(_DNAString.compare(i, 3, kmer3NuVector[j]) == 0){
				++triNuMap[kmer3NuVector[j]];
				break;
			}
		}

		for (int j = 0; j < 64; j++)
		{
			if(_AdditionalString.compare(i, 3, kmer3NuVector[j]) == 0){
				++triNuMap[kmer3NuVector[j]];
				break;
			}
		}
	}
	for (int i = 0; i < _DNAString.length() - 3; i++)
	{
		for (int j = 0; j < kmer4NuVector.size(); j++)
		{
			if(_DNAString.compare(i, KMer, kmer4NuVector[j]) == 0){
				++tetraNuMap[kmer4NuVector[j]];
				break;
			}
		}

		for (int j = 0; j < kmer4NuVector.size(); j++)
		{
			if(_AdditionalString.compare(i, KMer, kmer4NuVector[j]) == 0){
				++tetraNuMap[kmer4NuVector[j]];
				break;
			}
		}
	}

	for (int i = 0; i < V; i++)
	{
		string XYZW = kmer4NuVector[i]; //f XYZW
		string YZ = kmer4NuVector[i].substr(1,2);

		string XYZ = kmer4NuVector[i].substr(0,3);
		string YZW = kmer4NuVector[i].substr(1,3);

		double dividor = (triNuMap[XYZ] * triNuMap[YZW]);
		if (dividor == 0)
		{
			_vector[i] = 0;
		}
		else
		{
			_vector[i] = (double)(tetraNuMap[XYZW] * diNuMap[YZ])/dividor;
		}
	}
	
}

void GenerateHybridVector(double *_vector, string _DNAString, string _AdditionalString){
	if(signalNames[signalId] == " - Hybrid GC_FOM - "){
		double gcVector[V];
		GenerateGCContentVector(gcVector, _DNAString, _AdditionalString);

		double fomVector[V];
		GenerateFOMVector(fomVector, _DNAString, _AdditionalString);

		//GC + FOM
		for (int i = 0; i < V; i++)
		{
			// _vector[i] = fomVector[i] * gcVector[0];
			_vector[i] = pow(fomVector[i], 0.8) * gcVector[0];
			//_vector[i] = pow(fomVector[i], 0.7) * gcVector[0];
			//_vector[i] = pow(fomVector[i], 0.6) * gcVector[0];
			// _vector[i] = pow(fomVector[i], 0.5) * gcVector[0];
			// _vector[i] = pow(fomVector[i], 0.4) * gcVector[0];
			// _vector[i] = pow(fomVector[i], 0.2) * gcVector[0];

			if(isnan(_vector[i]))
				_vector[i] = 0;
		}
		
	}
	else if(signalNames[signalId] == " - Hybrid GC_SOM - ") {
		double gcVector[V];
		GenerateGCContentVector(gcVector, _DNAString, _AdditionalString);
		
		double somVector[V];
		GenerateSOMVector(somVector, _DNAString, _AdditionalString);

		//GC + SOM
		for (int i = 0; i < V; i++)
		{
			// _vector[i] = somVector[i] * gcVector[0];
			_vector[i] = pow(somVector[i], 0.5) * gcVector[0];
			// _vector[i] = pow(somVector[i], 0.2) * gcVector[0];

			if(isnan(_vector[i]))
				_vector[i] = 0;
		}
	}
	else if(signalNames[signalId] == " - Hybrid 4mer_GC_FOM - ") {
		double _4merVector[V];
		GenerateGCContentVector(_4merVector, _DNAString, _AdditionalString);

		double gcVector[V];
		GenerateGCContentVector(gcVector, _DNAString, _AdditionalString);
		
		double fomVector[V];
		GenerateFOMVector(fomVector, _DNAString, _AdditionalString);

		for (int i = 0; i < V; i++)
		{
			// _vector[i] = _4merVector[i] + fomVector[i] + gcVector[0];
			// _vector[i] = _4merVector[i] + fomVector[i]/20 + gcVector[0]/5;
			//_vector[i] = _4merVector[i] + fomVector[i]/40 + gcVector[0]/10;
			// _vector[i] = _4merVector[i] + fomVector[i]/50 + gcVector[0]/20;
			_vector[i] = _4merVector[i] + fomVector[i]/100 + gcVector[0]/40;

			if(isnan(_vector[i]))
				_vector[i] = 0;
		}
	}
	else if(signalNames[signalId] == " - Hybrid 4mer_GC_SOM - ") {
		double _4merVector[V];
		GenerateGCContentVector(_4merVector, _DNAString, _AdditionalString);

		double gcVector[V];
		GenerateGCContentVector(gcVector, _DNAString, _AdditionalString);
		
		double somVector[V];
		GenerateSOMVector(somVector, _DNAString, _AdditionalString);

		for (int i = 0; i < V; i++)
		{
			// _vector[i] = _4merVector[i] + somVector[i] + gcVector[0];
			// _vector[i] = _4merVector[i] + somVector[i]/20 + gcVector[0]/5;
			// _vector[i] = _4merVector[i] + somVector[i]/40 + gcVector[0]/10;
			// _vector[i] = _4merVector[i] + somVector[i]/50 + gcVector[0]/20;
			_vector[i] = _4merVector[i] + somVector[i]/100 + gcVector[0]/40;

			if(isnan(_vector[i]))
				_vector[i] = 0;
		}
	}

}

