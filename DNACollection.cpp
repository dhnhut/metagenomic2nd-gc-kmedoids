#include <fstream>
#include "DNACollection.h"
#include <sstream>

DNACollection::DNACollection(void)
{
	pFirst = NULL;
	pLast = NULL;
	numOfSequence = 0;
}

DNACollection::~DNACollection(void)
{
	//DNASequence *current = pFirst;
	//while (current != NULL)
	//{
	//	DNASequence *target = current;
	//	current = current -> pCollectionNext;
	//	delete target;
	//}
}

void DNACollection::AppendDNA(DNASequence* sequence) {
	if(pLast == NULL) {
		pFirst = sequence;
		pLast = sequence;
	} else {
		pLast -> pNext = sequence;
		pLast -> pCollectionNext = sequence;
		pLast = sequence;
	}
	numOfSequence++;
}

void DNACollection::ExportClusteringFile(){
	string fullFileName = "";
	if(runtimes == "")
		fullFileName = ResultsFolderName + "/" + fileName + signalNames[signalId] + "result.rawdata";
	else
		fullFileName = ResultsRunTimeFolderName + runtimes + "/" + fileName + signalNames[signalId] + "result.rawdata";

	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);

	DNASequence *current = pFirst;
	while (current != NULL)
	{
		ostringstream ss;
		ss << current -> clusterId;

		myfile << ss.str() << ",";
		current = current -> pCollectionNext;
	}

	myfile.close();
}

void DNACollection::Export4merVectorsFile(){
	string fullFileName = VectorFolderName + fileName + " - 4mer - Vectors.txt";
	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);

	int count = 0;
	DNASequence *current = pFirst;
	string result = "";
	while (current != NULL)
	{
		result = current -> Id();
		int length = (sizeof(current -> _kmerVector)/sizeof(*current -> _kmerVector));
		for (int i = 0; i < length; i++)
		{
			ostringstream ss;
			ss << current -> _kmerVector[i];
			result += " " + ss.str();
		}
		current = current -> pCollectionNext;
		++count;
		myfile << result << "\n";
	}

	myfile.close();
}

void DNACollection::ExportGCVectorsFile(){
	string fullFileName = VectorFolderName + fileName + " - GC - Vectors.txt";
	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);

	int count = 0;
	DNASequence *current = pFirst;
	string result = "";
	while (current != NULL)
	{
		result = current -> Id();
		int length = (sizeof(current -> _gcVector)/sizeof(*current -> _gcVector));
		for (int i = 0; i < length; i++)
		{
			ostringstream ss;
			ss << current -> _gcVector[i];
			result += " " + ss.str();
		}
		current = current -> pCollectionNext;
		++count;
		myfile << result << "\n";
	}

	myfile.close();
}

void DNACollection::ExportChartFile() {
	int arrClustSize[15];
	for (int i = 0; i < 15; ++i)
	{
		arrClustSize[i] = arrClustersSize[fileIndex][i];
	}

	int totalReads = arrTotalReads[fileIndex];

	string strResult = "";
	vector<string> result(257);
	vector<string> subResult(257);
	vector<string> subHeaderResult(totalSpecies);
	string readColors = "var colors = [";
	vector<string> subReadColors(totalSpecies);
	string colors[] = {"'red'", "'green'", "'blue'", "'yellow'", "'brown'"};

	result[0] = "var vectors = [['Reads'";

	int count = 0;
	int currentSpecies = 0;
	int bottomCount = 0;
	int topCount = linesPerSpecies;
	bool jumpedToNextSpecies = false;
	DNASequence *current = pFirst;
	
	//header, fields name, ....
	while (current != NULL && currentSpecies != totalSpecies)
	{
		if(bottomCount <= count && count < topCount) {
			jumpedToNextSpecies = false;
			result[0] += ",'" + current -> Id() + "'";
			subHeaderResult[currentSpecies] += ",'" + current -> Id() + "'";
			readColors += colors[currentSpecies] + ",";
			subReadColors[currentSpecies] += colors[currentSpecies] + ",";
		} else if (!jumpedToNextSpecies) {
			jumpedToNextSpecies = true;
			currentSpecies++;
			bottomCount += arrClustSize[currentSpecies];
			topCount += arrClustSize[currentSpecies];
		}
		current = current -> pCollectionNext;
		count++;
	}

	//vector values
	for (int i = 0; i < V; i++)
	{
		current = pFirst;

		ostringstream ss;
		ss << i;
		result[i + 1] = "['" + ss.str() + "'";
		count = 0;
		currentSpecies = 0;
		bottomCount = 0;
		topCount = linesPerSpecies;
		jumpedToNextSpecies = false;

		while (current != NULL && currentSpecies != totalSpecies)
		{
			if(bottomCount <= count && count < topCount) {
				jumpedToNextSpecies = false;
				ostringstream ss;
				ss << current -> _gcVector[i];
				result[i + 1] += "," + ss.str();
			} else if (!jumpedToNextSpecies) { //next species
				jumpedToNextSpecies = true;
				currentSpecies++;
				bottomCount += arrClustSize[currentSpecies];
				topCount += arrClustSize[currentSpecies];
			}
			current = current -> pCollectionNext;
			count++;
		}
	}

	strResult += ExportSubChartString(result, readColors, false);
	strResult += "\nvar subvectors = [";
	//export substrings
	current = pFirst;
	count = 0;
	currentSpecies = 0;
	bottomCount = 0;
	topCount = linesPerSpecies;
	jumpedToNextSpecies = false;
	for (int i = 0; i < V + 1; i++)
	{
		ostringstream ss;
		ss << i;
		subResult[i] = "['" + ss.str() + "'";
	}
	while (current != NULL && currentSpecies != totalSpecies)
	{
		if(bottomCount <= count && count < topCount) {
			jumpedToNextSpecies = false;
			for (int i = 0; i < V; i++)
			{
				ostringstream ss;
				ss << i;
				subResult[i + 1] += "," + ss.str();
			}
		} else if (!jumpedToNextSpecies) { //next species
			subHeaderResult[currentSpecies] = "vectors : [['Reads'" + subHeaderResult[currentSpecies];
			subResult[0] = subHeaderResult[currentSpecies];
			subReadColors[currentSpecies] = "colors : [" + subReadColors[currentSpecies];
			strResult += ExportSubChartString(subResult, subReadColors[currentSpecies], true);
			//reset
			for (int i = 0; i < V + 1; i++)
			{
				ostringstream ss;
				ss << i;
				subResult[i] = "['" + ss.str() + "'";
			}

			jumpedToNextSpecies = true;
			currentSpecies++;
			bottomCount += arrClustSize[currentSpecies];
			topCount += arrClustSize[currentSpecies];
		}
		current = current -> pCollectionNext;
		count++;
		
	}
	strResult += "];";

	string fullFileName = ResultsFolderName + fileName + signalNames[signalId] + "Visual.js";
	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);
	myfile << "var title = '" << fileName << " vector';";
	myfile << strResult;
	myfile.close();
}

string DNACollection::ExportSubChartString(vector<string> result, string readColors, bool isSubstring) {
	string strResult = "\n\n\n";
	string endLine = ";";
	if(isSubstring) {
		endLine = ",";
		strResult += "{";
	}
	for (int i = 0; i <= V; i++)
	{
		strResult += result[i] + "],";
	}
	strResult += "]" + endLine + "\n\n";
	strResult += readColors + "]" + endLine;
	if(isSubstring) {
		strResult += "},";
	}
	return strResult;
}