#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits.h>
#include <float.h> 
#include <math.h>
#include "DistanceCalculator.h"
#include "DNAKMean.h"

int GetInfo(Distance* _pFirst)
{
	double min = DBL_MAX;
	Distance *current = _pFirst;
	int minIndex = -1;
	int index = -1;
	while (current != NULL)
	{
		index++;
		if(min > current -> distance)
		{
			min = current -> distance;
			//pMin = current;
			minIndex = index;
		}
		current = current -> pNext;
	}
	return minIndex;
}

vector<DNACenter> Random_Center(DNACollection* collection){
	int totalReads = arrTotalReads[fileIndex];
	vector<DNACenter> pCenters(totalSpecies);
	vector<int> positions(totalSpecies);

	for (int i = 0; i < totalSpecies; ++i)
	{
		srand(time(NULL));
		positions[i] = rand() % totalReads + 1;  //number between 1 and totalReads

		//make sure no duplicate center
		for (int j = 0; j < i; ++j)
		{
			if (positions[i] == positions[j])
			{
				--i;
				break;
			}
		}
	}

	// positions[0] = 1;
	// positions[1] = totalReads;

	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		for (int j = 0; j < totalSpecies; ++j)
		{
			if (positions[j] == i + 1)
			{
				pCenters[j] = DNACenter(*(currentSequence));
				break;
			}
		}
		currentSequence = currentSequence -> pNext;
	}

	return pCenters;
}

void DNA_Kmean(DNACollection* collection) {
	int totalPoints = 0; //number of elements
	totalPoints = collection->numOfSequence;

	// DNACenter pCenters[totalSpecies] = { DNACenter(*(collection->pFirst)), DNACenter(*(collection->pLast)) };

	vector<DNACenter> pCenters = Random_Center(collection);
	
	bool endLoop = true;
	int countLoop = 0;
	do
	{
		countLoop++;
		cout << "Loop: " << countLoop <<" - ";

		vector<DNACenter> pC(totalSpecies);
		for (int i = 0; i < totalSpecies; i++)
		{
			pC[i] = pCenters[i];
			pC[i].pFrist = NULL;
		}

		DNASequence* currentSequence = collection -> pFirst;
		for (int j = 0; j < totalPoints; j++) {

			//calculate distance form all points to all centers
			DistanceList distanceList = DistanceList(totalSpecies);
			Distance *currentDis = distanceList.pFirst;
			int centerIndex = 0;
			while (currentDis != NULL)
			{
				double distance = calculateDistance(
					distanceFuncs, 
					currentSequence -> _vector, 
					pC[centerIndex].point._vector,
					currentSequence -> vectorMedian, 
					pC[centerIndex].point.vectorMedian
				);

				currentDis -> distance = distance;
				currentDis = currentDis->pNext;
				centerIndex += 1;
			}
			int nearestCenterIndex = GetInfo(distanceList.pFirst);

			pC[nearestCenterIndex].Add(currentSequence, nearestCenterIndex);

			currentSequence = currentSequence -> pCollectionNext;
		}

		//update centers
		endLoop = true;
		for (int j = 0; j < totalSpecies; j++)
		{
			pC[j].UpdateCenter();
			for(int i = 0; i < V; i++) {
				
				if(pC[j].point._vector[i] != pCenters[j].point._vector[i]) {
					pCenters[j].point._vector[i] = pC[j].point._vector[i];
					endLoop = false;
				}
			}			
		}
		//update cluster
		if(!endLoop)
		{
			for (int j = 0; j < totalSpecies; j++)
			{
				pC[j].UpdateCluster(&pCenters[j]);
			}
		}
	} while (!endLoop && countLoop < maxKmeanLoop);

	cout<<"\n";
}