#include "evaluation.h"
#include "DNACollection.h"
#include "DNAReadFile.h"
// #include "DNAKMean.h"
#include "DNAKMedoids.h"
#include <iostream>

using namespace std;

int numOfSignals = sizeof(signalNameIds)/sizeof(*signalNameIds);
int numOfFiles = sizeof(fileNames)/sizeof(*fileNames);

void RunSpecified(int times){
	cout<<"Filename: " << fileName << "\n";
	cout<<"Signal: " << signalNames[signalId] << "\n";
	
	DNACollection collection;

	// if(times == 1){
		collection = ReadDNA();
		collection.Export4merVectorsFile();
		collection.ExportGCVectorsFile();
	// 	// collection.ExportChartFile();
	// }
	// else{
	// 	collection = ReadVector();
	// }

	// collection = ReadVector();

	// DNA_Kmean(&collection);

	GenDistanceMatrix(&collection);
	DNA_KMedoids(&collection);

	collection.ExportClusteringFile();
	EvaluationResult();
}

void ChoiceSample() {

	//TODO: Get fileName, fileIndex, totalSpecies
	int choice = -1;

	for (int i = 0; i < numOfFiles; ++i) {
		cout << i << " - " << fileNames[i] << "\n";
	}

	do {
		cout << "Choice sample: ";
		cin >> choice;
	} while (choice < 0  || choice >= numOfFiles);

	fileName = fileNames[choice];
	fileIndex = choice;
	totalSpecies = arrTotalSpecies[choice];

	//TODO: Get signalId
	choice = -1;

	for (int i = 0; i < numOfSignals; ++i) {
		cout << i << " - " << signalNames[i] << "\n";
	}

	do {
		cout << "Choice signal: ";
		cin >> choice;
	} while (choice < 0  || choice >= numOfSignals);

	signalId = choice;

	RunSpecified(1);
}

void RunAll() {
	int skipCount = 0;
	int maxCount = 160;
	int runCount = 0;
	for (int times = 1; times < 2; ++times)
	{
		runCount = 0;
		stringstream ss;
		ss << times;
		runtimes = ss.str();
		cout<<"run "<<runtimes<<"\n";
		for (int i = 0; i < numOfFiles; ++i)
		{
			fileName = fileNames[i];
			fileIndex = i;
			totalSpecies = arrTotalSpecies[fileIndex];

			for (int j = 0; j < numOfSignals; ++j)
			{
				runCount++;
				if (runCount < skipCount)
					continue;
				else if(runCount > maxCount)
					break;

				signalId = j;

				bool isSkipSignal = false;
				for (int k = 0; k < numOfSignals; ++k)
				{
					if(signalId == skipSignals[k]){
						isSkipSignal = true;
						break;
					}
				}
				if(isSkipSignal)
					continue;

				cout << "\nRUNCOUNT: " << runCount << "\n";

				RunSpecified(times);
			}
		}
	}
}

int main()
{
	// int choice = -1;
	// do {
	// 	cout << "Enter 0 - to run all; 1 to run specified sample\nChoice: ";
	// 	cin >> choice;
	// } while (choice != 0 && choice != 1);
	int choice = 0;
	switch (choice) {
		case 0:
			RunAll();
			break;
		case 1:
			ChoiceSample();
			break;
	}

	cout<<"\n";
	return 0;
}